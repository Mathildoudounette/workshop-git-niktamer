﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HandleLight : MonoBehaviour
{
    public Light mainLight;
    public GameObject handleGrabbedDown;
    //float startLuminosity = 0;
    //float endLuminosity = 1;
    public Animation lightPopping;

    // Start is called before the first frame update
    void Start()
    {
        
        mainLight.intensity = 0;
        lightPopping.Stop();
    }

    // Update is called once per frame
    void Update()
    {
        
    }

    private void Awake()
    {
        GameObject.FindGameObjectWithTag("Handle");
    }

    public void turnOnLight()
    {
            mainLight.intensity = 1;
            lightPopping.Play();
            Debug.Log("it works");

        
    }
}

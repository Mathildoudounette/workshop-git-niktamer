﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SecretCode : MonoBehaviour
{
    public Text display;
    public string good;
    private bool finish = false;

    public void Pressed(string number)
    {
        StartCoroutine(Check());
        display.text = number;
        finish = true;

    }

    IEnumerator Check()
    {
        if (finish == true)
        {
            if (good == display.text)
            {
                yield return new WaitForSeconds(1f);
                display.text = "validé";
                yield return new WaitForSeconds(1f);
                display.text = "";
                yield return new WaitForSeconds(1f);
                
                GameObject.FindGameObjectWithTag("Player").GetComponent<Animator>().Play("FadeEnding") ;
                
                yield return new WaitForSeconds(2f);
                Application.Quit();


            }

            else
            {
                yield return new WaitForSeconds(1f);
                display.text = "error";
                yield return new WaitForSeconds(1f);
                display.text = "";
            }
            finish = false;
        }
        
    }
}
